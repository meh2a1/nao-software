# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 14:27:17 2018

@author: jaspe
"""

import speech_recognition as sr

r = sr.Recognizer()

with sr.Microphone() as source:
    print("Speak:")
    audio = r.listen(source)
    
try: 
    print("You said " + r.recognize_google(audio))
except sr.UnknownValueError:
    print("Could not understand audio")
except sr.RequestError as e:
    print("Could not request results; {0}".format(e))