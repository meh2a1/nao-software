
// rules for the right arm

// rules for the left arm

// rules for the right leg

// rules for the left leg

void StartingPose() {
  // rules for the right arm
  pose.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, 
  PoseRule.STRAIGHT_BELOW, 
  SimpleOpenNI.SKEL_RIGHT_ELBOW);
  pose.addRule(SimpleOpenNI.SKEL_RIGHT_ELBOW, 
  PoseRule.STRAIGHT_BELOW, 
  SimpleOpenNI.SKEL_RIGHT_SHOULDER);

  // rules for the left arm
  pose.addRule(SimpleOpenNI.SKEL_LEFT_HAND, 
  PoseRule.STRAIGHT_BELOW, 
  SimpleOpenNI.SKEL_LEFT_ELBOW);
  pose.addRule(SimpleOpenNI.SKEL_LEFT_ELBOW, 
  PoseRule.STRAIGHT_BELOW, 
  SimpleOpenNI.SKEL_LEFT_SHOULDER);

  // rules for the right leg
  pose.addRule(SimpleOpenNI.SKEL_RIGHT_KNEE, 
  PoseRule.STRAIGHT_BELOW, 
  SimpleOpenNI.SKEL_RIGHT_HIP);
  pose.addRule(SimpleOpenNI.SKEL_RIGHT_FOOT, 
  PoseRule.STRAIGHT_BELOW, 
  SimpleOpenNI.SKEL_RIGHT_KNEE);

  // rules for the left leg
  pose.addRule(SimpleOpenNI.SKEL_LEFT_KNEE, 
  PoseRule.STRAIGHT_BELOW, 
  SimpleOpenNI.SKEL_LEFT_HIP);
  pose.addRule(SimpleOpenNI.SKEL_LEFT_FOOT, 
  PoseRule.STRAIGHT_BELOW, 
  SimpleOpenNI.SKEL_LEFT_KNEE);
}

void LiftWater() {
  /* LIFT WATER 10-form  */
  // rules for the right arm
  liftWater.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, 
  PoseRule.STRAIGHT_INFRONT, 
  SimpleOpenNI.SKEL_RIGHT_ELBOW);
  liftWater.addRule(SimpleOpenNI.SKEL_RIGHT_ELBOW, 
  PoseRule.STRAIGHT_INFRONT, 
  SimpleOpenNI.SKEL_RIGHT_SHOULDER);
  liftWater.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, 
  PoseRule.BELOW, 
  SimpleOpenNI.SKEL_RIGHT_SHOULDER);

  // rules for the left arm
  liftWater.addRule(SimpleOpenNI.SKEL_LEFT_HAND, 
  PoseRule.STRAIGHT_INFRONT, 
  SimpleOpenNI.SKEL_LEFT_ELBOW);
  liftWater.addRule(SimpleOpenNI.SKEL_LEFT_ELBOW, 
  PoseRule.STRAIGHT_INFRONT, 
  SimpleOpenNI.SKEL_LEFT_SHOULDER);
  liftWater.addRule(SimpleOpenNI.SKEL_LEFT_HAND, 
  PoseRule.BELOW, 
  SimpleOpenNI.SKEL_LEFT_SHOULDER);

  // rules for the right leg
  liftWater.addRule(SimpleOpenNI.SKEL_RIGHT_KNEE, 
  PoseRule.RIGHT_OF, 
  SimpleOpenNI.SKEL_RIGHT_HIP);
  liftWater.addRule(SimpleOpenNI.SKEL_RIGHT_FOOT, 
  PoseRule.RIGHT_OF, 
  SimpleOpenNI.SKEL_RIGHT_KNEE);

  // rules for the left leg
  liftWater.addRule(SimpleOpenNI.SKEL_LEFT_KNEE, 
  PoseRule.LEFT_OF, 
  SimpleOpenNI.SKEL_LEFT_HIP);
  liftWater.addRule(SimpleOpenNI.SKEL_LEFT_FOOT, 
  PoseRule.LEFT_OF, 
  SimpleOpenNI.SKEL_LEFT_KNEE);
}

void RepulseMonkey() {
  /************* Repulse Monkey - RIGHT *************/
  // rules for the right arm
  repulseMonkey_right.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, 
  PoseRule.LEFT_OF, 
  SimpleOpenNI.SKEL_RIGHT_SHOULDER);
  repulseMonkey_right.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, 
  PoseRule.ABOVE, 
  SimpleOpenNI.SKEL_RIGHT_ELBOW);

  // rules for the left arm
  repulseMonkey_right.addRule(SimpleOpenNI.SKEL_LEFT_HAND, 
  PoseRule.LEFT_OF, 
  SimpleOpenNI.SKEL_LEFT_SHOULDER);
  repulseMonkey_right.addRule(SimpleOpenNI.SKEL_LEFT_HAND, 
  PoseRule.ABOVE, 
  SimpleOpenNI.SKEL_LEFT_ELBOW);
  repulseMonkey_right.addRule(SimpleOpenNI.SKEL_LEFT_HAND, 
  PoseRule.ABOVE, 
  SimpleOpenNI.SKEL_LEFT_SHOULDER);

  // rules for the right leg
  repulseMonkey_right.addRule(SimpleOpenNI.SKEL_RIGHT_KNEE, 
  PoseRule.RIGHT_OF, 
  SimpleOpenNI.SKEL_RIGHT_HIP);
  repulseMonkey_right.addRule(SimpleOpenNI.SKEL_RIGHT_FOOT, 
  PoseRule.RIGHT_OF, 
  SimpleOpenNI.SKEL_RIGHT_KNEE);

  // rules for the left leg
  repulseMonkey_right.addRule(SimpleOpenNI.SKEL_LEFT_KNEE, 
  PoseRule.LEFT_OF, 
  SimpleOpenNI.SKEL_LEFT_HIP);
  repulseMonkey_right.addRule(SimpleOpenNI.SKEL_LEFT_FOOT, 
  PoseRule.LEFT_OF, 
  SimpleOpenNI.SKEL_LEFT_KNEE);


  /************* Repulse Monkey - LEFT *************/
  // rules for the right arm
  repulseMonkey_left.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, 
  PoseRule.LEFT_OF, 
  SimpleOpenNI.SKEL_RIGHT_SHOULDER);
  repulseMonkey_left.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, 
  PoseRule.ABOVE, 
  SimpleOpenNI.SKEL_RIGHT_ELBOW);
  repulseMonkey_left.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, 
  PoseRule.ABOVE, 
  SimpleOpenNI.SKEL_RIGHT_SHOULDER);

  // rules for the left arm
  repulseMonkey_left.addRule(SimpleOpenNI.SKEL_LEFT_HAND, 
  PoseRule.LEFT_OF, 
  SimpleOpenNI.SKEL_LEFT_SHOULDER);
  repulseMonkey_left.addRule(SimpleOpenNI.SKEL_LEFT_HAND, 
  PoseRule.ABOVE, 
  SimpleOpenNI.SKEL_LEFT_ELBOW);

  // rules for the right leg
  repulseMonkey_left.addRule(SimpleOpenNI.SKEL_RIGHT_KNEE, 
  PoseRule.RIGHT_OF, 
  SimpleOpenNI.SKEL_RIGHT_HIP);
  repulseMonkey_left.addRule(SimpleOpenNI.SKEL_RIGHT_FOOT, 
  PoseRule.RIGHT_OF, 
  SimpleOpenNI.SKEL_RIGHT_KNEE);

  // rules for the left leg
  repulseMonkey_left.addRule(SimpleOpenNI.SKEL_LEFT_KNEE, 
  PoseRule.LEFT_OF, 
  SimpleOpenNI.SKEL_LEFT_HIP);
  repulseMonkey_left.addRule(SimpleOpenNI.SKEL_LEFT_FOOT, 
  PoseRule.LEFT_OF, 
  SimpleOpenNI.SKEL_LEFT_KNEE);
}


void brushKnee() {
  /********************Brush knee left*/
  // rules for the right arm
  brushKnee_left.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, PoseRule.STRAIGHT_SIDE, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
  brushKnee_left.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, PoseRule.RIGHT_OF, SimpleOpenNI.SKEL_LEFT_KNEE);

  // rules for the left arm
  brushKnee_left.addRule(SimpleOpenNI.SKEL_LEFT_HAND, PoseRule.RIGHT_OF, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
  brushKnee_left.addRule(SimpleOpenNI.SKEL_LEFT_HAND, PoseRule.BELOW, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
  brushKnee_left.addRule(SimpleOpenNI.SKEL_LEFT_HAND, PoseRule.RIGHT_OF, SimpleOpenNI.SKEL_TORSO);
  brushKnee_left.addRule(SimpleOpenNI.SKEL_LEFT_HAND, PoseRule.LEFT_OF, SimpleOpenNI.SKEL_RIGHT_ELBOW);
  
  // rules for the right leg
  brushKnee_left.addRule(SimpleOpenNI.SKEL_RIGHT_KNEE, PoseRule.RIGHT_OF, SimpleOpenNI.SKEL_TORSO);
  brushKnee_left.addRule(SimpleOpenNI.SKEL_RIGHT_FOOT, PoseRule.RIGHT_OF, SimpleOpenNI.SKEL_LEFT_KNEE);

  // rules for the left leg
  brushKnee_left.addRule(SimpleOpenNI.SKEL_LEFT_KNEE, PoseRule.LEFT_OF, SimpleOpenNI.SKEL_TORSO);
  brushKnee_left.addRule(SimpleOpenNI.SKEL_LEFT_FOOT, PoseRule.LEFT_OF, SimpleOpenNI.SKEL_LEFT_KNEE);
  
  /***************** brush knee right*/
  // rules for the right arm
  brushKnee_right.addRule(SimpleOpenNI.SKEL_LEFT_HAND, PoseRule.STRAIGHT_SIDE, SimpleOpenNI.SKEL_LEFT_SHOULDER);
  brushKnee_right.addRule(SimpleOpenNI.SKEL_LEFT_HAND, PoseRule.LEFT_OF, SimpleOpenNI.SKEL_RIGHT_KNEE);

  // rules for the left arm
  brushKnee_right.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, PoseRule.LEFT_OF, SimpleOpenNI.SKEL_LEFT_SHOULDER);
  brushKnee_right.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, PoseRule.BELOW, SimpleOpenNI.SKEL_LEFT_SHOULDER);
  brushKnee_right.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, PoseRule.LEFT_OF, SimpleOpenNI.SKEL_TORSO);
  brushKnee_right.addRule(SimpleOpenNI.SKEL_RIGHT_HAND, PoseRule.RIGHT_OF, SimpleOpenNI.SKEL_LEFT_ELBOW);
  
  // rules for the right leg
  brushKnee_right.addRule(SimpleOpenNI.SKEL_LEFT_KNEE, PoseRule.LEFT_OF, SimpleOpenNI.SKEL_TORSO);
  brushKnee_right.addRule(SimpleOpenNI.SKEL_LEFT_FOOT, PoseRule.LEFT_OF, SimpleOpenNI.SKEL_RIGHT_KNEE);

  // rules for the left leg
  brushKnee_right.addRule(SimpleOpenNI.SKEL_RIGHT_KNEE, PoseRule.RIGHT_OF, SimpleOpenNI.SKEL_TORSO);
  brushKnee_right.addRule(SimpleOpenNI.SKEL_RIGHT_FOOT, PoseRule.RIGHT_OF, SimpleOpenNI.SKEL_RIGHT_KNEE);
}


void test(){
  test.addRule_angle(SimpleOpenNI.SKEL_RIGHT_ELBOW, PoseRule.AT_ANGLE, SimpleOpenNI.SKEL_RIGHT_HAND, -10);
}

