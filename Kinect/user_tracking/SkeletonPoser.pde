class PoseRule { 
  int fromJoint;
  int toJoint;
  private int angle; //holds the angle between joints
  PVector fromJointVector;
  PVector toJointVector;
  SimpleOpenNI context;
  int jointRelation; // one of:
  static final int ABOVE     = 1;
  static final int BELOW     = 2;
  static final int LEFT_OF   = 3;
  static final int RIGHT_OF  = 4;
  static final int STRAIGHT_BELOW  = 5;
  static final int STRAIGHT_INFRONT  = 6;
  static final int STRAIGHT_SIDE = 7;
  static final int AT_ANGLE = 8;

  PoseRule(SimpleOpenNI tempContext, 
  int tempFromJoint, 
  int tempJointRelation, 
  int tempToJoint)
  {
    context = tempContext; 
    fromJoint = tempFromJoint;
    toJoint = tempToJoint;
    jointRelation = tempJointRelation;
    //angle = angleTemp;
    fromJointVector = new PVector(); 
    toJointVector = new PVector();
  }

  PoseRule(SimpleOpenNI tempContext, 
  int tempFromJoint, 
  int tempJointRelation, 
  int tempToJoint, 
  int tempAngle)
  {
    context = tempContext; 
    fromJoint = tempFromJoint;
    toJoint = tempToJoint;
    jointRelation = tempJointRelation;
    angle = tempAngle;
    fromJointVector = new PVector(); 
    toJointVector = new PVector();
  }

  boolean check(int userID) { 
    // populate the joint vectors for the user we're checking
    context.getJointPositionSkeleton(userID, fromJoint, fromJointVector);
    context.getJointPositionSkeleton(userID, toJoint, toJointVector);
    boolean result= false;
    switch(jointRelation) { 
    case ABOVE:
      result = (fromJointVector.y > toJointVector.y);
      break;
    case BELOW:
      result = (fromJointVector.y < toJointVector.y);
      break;
    case LEFT_OF:
      result = (fromJointVector.x < toJointVector.x);
      break;
    case RIGHT_OF:
      result = (fromJointVector.x > toJointVector.x);
      break;
    case STRAIGHT_BELOW:
      result = (fromJointVector.y < toJointVector.y) && 
        (abs(fromJointVector.x - toJointVector.x) < 80);
      break;
    case STRAIGHT_INFRONT:
      result = (abs(fromJointVector.y - toJointVector.y) < 150) && 
        (abs(fromJointVector.x - toJointVector.x) < 80);    
      break;
    case STRAIGHT_SIDE:
      result = (abs(fromJointVector.y - toJointVector.y) < 80);    
      break;

    case AT_ANGLE:
      float angleDeviation = abs(degrees(atan2(fromJointVector.y-toJointVector.y, fromJointVector.x-toJointVector.x)) - angle);
      result = (angleDeviation < 10);
      /*text("Angle measured: " + int(degrees(atan2(fromJointVector.y-toJointVector.y,
        fromJointVector.x-toJointVector.x))) + " deg", 500, 150);*/
      break;
    }
    return result;
  }
}

class SkeletonPoser { 
  SimpleOpenNI context;
  ArrayList rules;
  SkeletonPoser(SimpleOpenNI context) { 
    this.context = context;
    rules = new ArrayList();
  }

  void addRule(int fromJoint, int jointRelation, int toJoint) {  
    PoseRule rule = new PoseRule(context, fromJoint, jointRelation, toJoint);
    rules.add(rule);
  }

  void addRule_angle(int fromJoint, int jointRelation, int toJoint, int angle) {
    PoseRule rule = new PoseRule(context, fromJoint, jointRelation, toJoint, angle);
    rules.add(rule);
  }

  boolean check(int userID) { 
    boolean result = true; 
    for (int i = 0; i < rules.size (); i++) { 
      PoseRule rule = (PoseRule)rules.get(i); 
      result = result && rule.check(userID);
    }
    return result;
  }
}

