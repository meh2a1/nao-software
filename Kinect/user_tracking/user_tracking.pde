import SimpleOpenNI.*;
// import and declarations for minim: 
import ddf.minim.*;
//Minim minim;
//AudioPlayer player;

// declare our poser objects. In order of execution
SkeletonPoser pose;
SkeletonPoser liftWater;
SkeletonPoser repulseMonkey_right;
SkeletonPoser repulseMonkey_left;
SkeletonPoser brushKnee_left;
SkeletonPoser brushKnee_right;
SkeletonPoser wildHorse_left;
SkeletonPoser wildHorse_right;
SkeletonPoser test;
SimpleOpenNI  kinect;

StopWatchTimer stopwatch;

//Constants
final boolean DEBUG_MODE = true;

// Variables
boolean poseCorrect = false;
int poseNumber = 60;


void setup() { 
  size(1280, 480);
  kinect = new SimpleOpenNI(this);
  kinect.enableDepth();
  //kinect.enableRGB();
  kinect.enableUser();
  kinect.setMirror(true);

  // initialize the minim object
  //minim = new Minim(this);

  stopwatch = new StopWatchTimer();

  // and load the stayin alive mp3 file
  //player = minim.loadFile("Neelix.mp3");

  // initialize the pose object
  pose = new SkeletonPoser(kinect); 
  liftWater = new SkeletonPoser(kinect);
  repulseMonkey_right = new SkeletonPoser(kinect);
  repulseMonkey_left = new SkeletonPoser(kinect);
  brushKnee_left = new SkeletonPoser(kinect);
  brushKnee_right = new SkeletonPoser(kinect);
  test = new SkeletonPoser(kinect);
  //wildHorse_left = new SkeletonPoser(kinect);
  //wildHorse_right = new SkeletonPoser(kinect);



  StartingPose();
  LiftWater();
  RepulseMonkey();
  brushKnee();
  test();


  strokeWeight(5);
}


void draw() { 
  background(0);
  kinect.update();
  image(kinect.depthImage(), 0, 0);
  //image(kinect.rgbImage(), 640, 0);
  IntVector userList = new IntVector();
  kinect.getUsers(userList);

  if (userList.size() > 0) {
    int userId = userList.get(0);
    if ( kinect.isTrackingSkeleton(userId)) {
      switch(poseNumber) {
      case 0: // starting pos
        poseCorrect = pose.check(userId);
        break;

      case 1: // LIFT WATER (1/10)  
        poseCorrect = liftWater.check(userId);
        break;

      case 2: //repulse monkey right
        poseCorrect = repulseMonkey_right.check(userId);
        break;

      case 3: //repulse monkey left
        poseCorrect = repulseMonkey_left.check(userId);
        break;

      case 4: //brush knee left
        poseCorrect = brushKnee_left.check(userId);
        break;

      case 5: //brush knee right
        poseCorrect = brushKnee_right.check(userId);
        break;

      case 60: //test pose
        poseCorrect = test.check(userId);
        break;
      }

      if (!poseCorrect) {
        stroke(255, 0, 0);
        stopwatch.stop();
      } else {
        stroke(255);
        println("GOOD!");

        stopwatch.start();
        if (stopwatch.second() > 1000) {  //if pose is correct for x seconds go to next pose
          stopwatch.stop();
          //++poseNumber;
        }
      }
    }

    if (DEBUG_MODE) {
      drawSkeleton(userId);
      calcAngles(userId);
    }
  }
}


void drawSkeleton(int userId) {
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_ELBOW, SimpleOpenNI.SKEL_LEFT_HAND);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_LEFT_SHOULDER);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_LEFT_ELBOW);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_HEAD, SimpleOpenNI.SKEL_NECK);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_RIGHT_ELBOW);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW, SimpleOpenNI.SKEL_RIGHT_HAND);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_LEFT_HIP);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_HIP, SimpleOpenNI.SKEL_LEFT_KNEE);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_KNEE, SimpleOpenNI.SKEL_LEFT_FOOT);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_RIGHT_HIP);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_HIP, SimpleOpenNI.SKEL_RIGHT_KNEE);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_KNEE, SimpleOpenNI.SKEL_RIGHT_FOOT);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_HIP, SimpleOpenNI.SKEL_LEFT_HIP);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_HIP, SimpleOpenNI.SKEL_LEFT_HIP);
}


void calcAngles(int userId) {
  fill(255, 255, 0);
  scale(1.5);
  //left forearm
  float angle = degrees(getJointAngle(userId, SimpleOpenNI.SKEL_LEFT_ELBOW, SimpleOpenNI.SKEL_LEFT_HAND));
  text("Left Forearm: " + int(angle) + " deg", 500, 15);

  //left upper arm
  angle = degrees(getJointAngle(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_LEFT_ELBOW));
  text("Left Upper arm: " + int(angle) + " deg", 500, 30);

  //left upper leg
  angle = degrees(getJointAngle(userId, SimpleOpenNI.SKEL_LEFT_HIP, SimpleOpenNI.SKEL_LEFT_KNEE));
  text("Left Upper leg: " + int(angle) + " deg", 500, 45);

  //left lower leg
  angle = degrees(getJointAngle(userId, SimpleOpenNI.SKEL_LEFT_KNEE, SimpleOpenNI.SKEL_LEFT_FOOT));
  text("Left lower leg: " + int(angle) + " deg", 500, 60);

  //right forearm
  angle = degrees(getJointAngle(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW, SimpleOpenNI.SKEL_RIGHT_HAND));
  text("Right Forearm: " + int(angle) + " deg", 500, 75);

  //right upper arm
  angle = degrees(getJointAngle(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_RIGHT_ELBOW));
  text("Right Upper arm: " + int(angle) + " deg", 500, 90);

  //right upper leg
  angle = degrees(getJointAngle(userId, SimpleOpenNI.SKEL_RIGHT_HIP, SimpleOpenNI.SKEL_RIGHT_KNEE));
  text("Right Upper leg: " + int(angle) + " deg", 500, 105);

  //right lower leg
  angle = degrees(getJointAngle(userId, SimpleOpenNI.SKEL_RIGHT_KNEE, SimpleOpenNI.SKEL_RIGHT_FOOT));
  text("Right lower leg: " + int(angle) + " deg", 500, 120);
}


void onNewUser(SimpleOpenNI curContext, int userId) {
  println("onNewUser - userId: " + userId);
  println("\tstart tracking skeleton");

  kinect.startTrackingSkeleton(userId);
}


void onLostUser(SimpleOpenNI curContext, int userId) {
  println("onLostUser - userId: " + userId);
}

/*
void onVisibleUser(SimpleOpenNI curContext, int userId) {
 //println("onVisibleUser - userId: " + userId);
 }*/


float getJointAngle(int userId, int jointID1, int jointID2) {
  PVector joint1 = new PVector();
  PVector joint2 = new PVector();
  kinect.getJointPositionSkeleton(userId, jointID1, joint1);
  kinect.getJointPositionSkeleton(userId, jointID2, joint2);
  return atan2(joint1.y-joint2.y, joint1.x-joint2.x);
}

