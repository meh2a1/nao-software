class StopWatchTimer {
  int startTime = 0;
  boolean running = false;  
  
    void start() {
        if (!running){
          startTime = millis();
          running = true;
        }
    }
    
    void stop() {
        running = false;
    }
    
    int getElapsedTime() {
        return (millis() - startTime);
    }
    
    int second() {
      return (getElapsedTime() / 1000) % 60;
    }
    
    int minute() {
      return (getElapsedTime() / (1000*60)) % 60;
    }
    
    int hour() {
      return (getElapsedTime() / (1000*60*60)) % 24;
    }
}
